package model;

public class Asteroid3 extends Asteroid {

    private int type = 1;

    public Asteroid3(int x, int y) {
        super(x, y);
        this.h = 60;
        this.w = 60;
        this.speedY = 0.15f;
        super.setType(3);
    }
}