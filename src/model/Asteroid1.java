package model;

public class Asteroid1 extends Asteroid {
    private int type = 1;
    public Asteroid1(int x, int y) {
        super(x, y);
        this.h = 20;
        this.w = 20;
        this.speedY = 0.3f;
        super.setType(1);
    }
}
