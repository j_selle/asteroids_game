package model;

import javax.swing.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {
    // FINALS
    public static final int WIDTH = 600;
    public static final int HEIGHT = 800;

    // Eigenschaften
    private List<Asteroid> asteroids = new LinkedList<>();
    private Player player;
    private Random random = new Random();
    private long startTime;
    private int level = 1;
    private Background background;

    // Konstruktoren
    public Model() {
//        this.asteroids.add(new Asteroid1(100, 20));
//        this.asteroids.add(new Asteroid1(400, 20));
        this.player = new Player(300, 500);
        this.background = new Background();
        this.startTime = System.nanoTime();
    }

    // Methoden
    public void update (long elapsedTime) {

        background.update(elapsedTime);

        for (Asteroid asteroid : asteroids){
            asteroid.update(elapsedTime);
            if (asteroids.size() > 30) {
                asteroids.remove(0);
            }
            if (asteroid.collision(player)) {
                player.decrementLives();
            }
            if (player.getLives() == 0) {
                endGame();
                System.exit(0);

            }

        }

        if(System.nanoTime()/1000000 - startTime/1000000 < 40000 ) {

            if (random.nextInt() % 100 == 0) {
                this.asteroids.add(new Asteroid1(random.nextInt(WIDTH), 0));
            }
            if (random.nextInt() % 150 == 0) {
                this.asteroids.add(new Asteroid2(random.nextInt(WIDTH), 0));
            }
            if (random.nextInt() % 200 == 0) {
                this.asteroids.add(new Asteroid3(random.nextInt(WIDTH), 0));
            }
        }
        else {
            level = 2;
            if (random.nextInt() % 20 == 0) {
                this.asteroids.add(new Asteroid1(random.nextInt(WIDTH), 0));
            }
            if (random.nextInt() % 50 == 0) {
                this.asteroids.add(new Asteroid2(random.nextInt(WIDTH), 0));
            }
            if (random.nextInt() % 40 == 0) {
                this.asteroids.add(new Asteroid3(random.nextInt(WIDTH), 0));
            }

        }
    }

    private static void endGame() {
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException ex) {}
        JOptionPane.showMessageDialog(null, "Game Over!");

    }



    // Setter + Getter
    public List<Asteroid> getAsteroids() {
        return asteroids;
    }

    public Player getPlayer() {
        return player;
    }

    public Background getBackground() {
        return background;
    }

    public int getLevel() {
        return level;
    }
}
