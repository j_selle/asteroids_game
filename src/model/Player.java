package model;

public class Player {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private int lives;
    private long lastColTime = 0;

    // Konstruktoren
    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 45;
        this.w = 45;
        this.lives = 3;
    }

    // Methoden
    public void move (int dx, int dy) {
    /*Player can not cross over edges of frame, no change in coordinates if move would cause this */
        this.x += ((this.x + dx < Model.WIDTH - this.w) && (this.x + dx > this.w) ? dx : 0 );
        this.y += ((this.y + dy < Model.HEIGHT - this.h) && (this.y + dy > this.h) ? dy : 0 );
//        this.x += dx;
//        this.y += dy;
    }


    // Getter + Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public int getLives() {
        return this.lives;
    }

    public void decrementLives() {
//        System.out.println("Last: " + lastColTime + " Now: " + (System.nanoTime()/1000000));
        if ((((System.nanoTime()/1000000) - lastColTime) > 2000)) {
            this.lives--;
            lastColTime = System.nanoTime()/1000000;
        }
    }

}
